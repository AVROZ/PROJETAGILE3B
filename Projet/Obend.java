import java.awt.event.*;

/**
* La classe <code>Obend</code> Observateur de la class Endgame
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Obend implements ActionListener{
	@Override
	/**
	* Ramene le joueur au menu du jeux pour qu'il ai le choix de relancer ou de quitter
	*/
	public void actionPerformed(ActionEvent eve){
		if(eve.getSource()==Endgame.menu){
			Endgame.endgame.dispose();
			Jeux.fenetre.principal();   
		}
	}
}