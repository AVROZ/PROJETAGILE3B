import javax.swing.*;
import javax.swing.ImageIcon;
import java.awt.*;
import java.util.Random;

/**
* La classe <code>Ecran</code> Ecran du jeu plus le jeu en lui même.
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Ecran implements API {
	/**
	* Verifie si la case à déjà été prise en compte dans le groupe
	*/
	private static int[][] verif;
	/**
	*capture les pion selectionner pour d'autre méthode
	*/
	public static char[][] capture;
	/**
	* Ecran de jeu
	*/
	public static JFrame fj;
	/**
	* permet de d'attribuet les icons des pion sur l'ecran
	*/
	private static JLabel[][] fond;
	/**
	* Bouton pause
	*/
	public static  JButton pause = new JButton("pause");
	/**
	* Appel de la class pause
	*/
	public static Pause m = new Pause(fj);
	/**
	* Permet de diferencier le type de jeu entre aleatoire et predefinie 
	*/
	public static int choix=0;
	/**
	* Appel la class grille pour l'aleatoires
	*/ 
	private Grille grille;
	/**
	* Tableau de char pour mettre le tableau generé aleatoire ou non
	*/
	private static char[][] pion;
	/**
	* Pour recuperer le pion concerné a une action
	*/
	private static int[][] recup;
	/**
	* nombre de pion dans un groupe
	*/
	private static int compteur;
	/**
	* pion vide
	*/
	private static ImageIcon vide = new ImageIcon(" ");
	/**
	* pion banane
	*/
	private static	ImageIcon banane = new ImageIcon("./image/banane.png");
	/**
	* Pion fraise
	*/
	private static	ImageIcon fraise = new ImageIcon("./image/fraise.png");
	/**
	* pion pêche
	*/
	private static	ImageIcon peche = new ImageIcon("./image/peche.png");
	/**
	* pion kiwi
	*/
	private static	ImageIcon kiwi = new ImageIcon("./image/kiwi.png");
	/**
	* pion framboise
	*/
	private static	ImageIcon framboise = new ImageIcon("./image/framboise.png");
	/**
	* pion orange
	*/
	private static	ImageIcon orange = new ImageIcon("./image/orange.png");
	/**
	* point gagné par coup
	*/
	private static int point = 0;
	/**
	* Valeur stockant score total
	*/
	public static int score;
	/**
	* Nombre de colonnes dans la grille
	*/
	public static int size_x = 15;
	/**
	* Nombre de lignes dans la grille
	*/
	public static int size_y = 10;
	/**
	* Nombre de pions différents
	*/
	public static int nb_pion = 3;
	/**
	* Verif de fin
	*/
	public static int end = 0;
	/**
	* Score total en String
	*/
	private static String s_score;
	/**
	* Valeur stockant la coordonné x selectionné aléatoirement
	*/
	public static int a;
	/**
	* Valeur stockant la coordonné y selectionné aléatoirement
	*/
	public static int b;

	/**
	* Tableau de char pour mettre le tableau generé aleatoire ou non
	*/
	//private static char[][] grillebot;


	/**
	* Verifie la fin de jeu
	*/
	public static void fin(){
		int i,j;
		boolean status,win;
		win=false;
		for (i=0;i<size_y;i++){
			status=ligneverif(i);
			if(status==false){
				for(j=0;j<size_x;j++){
					if(capture[i][j]!=' '){
						if(i>0 && capture[i][j]==capture[i-1][j] || i<(size_y-1) && capture[i][j]==capture[i+1][j]){
							win=true;
						}

						if(j>0 && capture[i][j]==capture[i][j-1] || j<(size_x-1) && capture[i][j]==capture[i][j+1]){
							win=true;
						}
					}
				}
			}
		}
		if(win==false){
			end=1;
		}
	}

	/**
	* Compte les pions d'un groupe
	*/
	public static int getCompteur (){
		return compteur;
	}


	/**
	* Constructeur qui s'occupe de l'affichage de l'ecran et son contenue
	*/
	public Ecran(){
		super();
		verif = new int[size_y][size_x];
		capture = new char[size_y][size_x];
		fond = new JLabel[size_y][size_x];
		pion = new char[size_y][size_x];
		recup = new int[size_x*size_y][2];
		
		if(choix==0){
			this.grille=new Grille(size_x, size_y, nb_pion);
			pion=this.grille.pion;

		}
		if(choix==1){
			pion=Fichier.jeux;
		}

		this.fj=new JFrame();

		this.point=0;
		this.end=0;
		pause.addActionListener(new Obe(fj));
		fj.setIconImage(Toolkit.getDefaultToolkit().getImage("./image/Kitty_kitty.png"));
		fj.setTitle("Same Game");
		Color lila=Fenetre.lila;

		JPanel panneau= new JPanel();
		JPanel pb=new JPanel();
		pb.add(pause);
		fj.setSize(50 + size_x*50, 100 + size_y*50);
		fj.setResizable(false);
		fj.setLocation(0, 0);
		pause.setBackground(lila);
		fj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		GridLayout jeux = new GridLayout(size_y, size_x);
		panneau.setBackground(Color.WHITE);
		panneau.setLayout(jeux);

		for(int i=0;i<size_y;i++){
			for(int y=0;y<size_x;y++) {
				if(pion[i][y]=='R') {//rose
        			fond[i][y]=new JLabel(banane, JLabel.CENTER);
        			fond[i][y].setOpaque(false);
        			fond[i][y].setPreferredSize(new Dimension(50,50));
        			panneau.add(fond[i][y]);
        			if(Fenetre.modedejeu==1) {
        				fond[i][y].addMouseListener(new Observateur2(fond[i][y],i,y));
        			}
        			capture[i][y]=pion[i][y];
        		}

				if(pion[i][y]=='V') {//cyan
					fond[i][y]=new JLabel(fraise, JLabel.CENTER);
					fond[i][y].setOpaque(false);
					fond[i][y].setPreferredSize(new Dimension(50,50));
					panneau.add(fond[i][y]);
					capture[i][y]=pion[i][y];
					if(Fenetre.modedejeu==1) {
						fond[i][y].addMouseListener(new Observateur2(fond[i][y],i,y));
					}
					
				}

				if(pion[i][y]=='B') {//jaune
					fond[i][y]=new JLabel(peche, JLabel.CENTER);
					fond[i][y].setOpaque(false);
					fond[i][y].setPreferredSize(new Dimension(50,50));
					panneau.add(fond[i][y]);
					capture[i][y]=pion[i][y];
					if(Fenetre.modedejeu==1) {
						fond[i][y].addMouseListener(new Observateur2(fond[i][y],i,y));
					}
					
				}
				
				if(pion[i][y]=='K') {//vert
					fond[i][y]=new JLabel(kiwi, JLabel.CENTER);
					fond[i][y].setOpaque(false);
					fond[i][y].setPreferredSize(new Dimension(50,50));
					panneau.add(fond[i][y]);
					if(Fenetre.modedejeu==1) {
						fond[i][y].addMouseListener(new Observateur2(fond[i][y],i,y));
					}
					capture[i][y]=pion[i][y];
				}
				
				if(pion[i][y]=='F') {//rouge
					fond[i][y]=new JLabel(framboise, JLabel.CENTER);
					fond[i][y].setOpaque(false);
					fond[i][y].setPreferredSize(new Dimension(50,50));
					panneau.add(fond[i][y]);
					if(Fenetre.modedejeu==1) {
						fond[i][y].addMouseListener(new Observateur2(fond[i][y],i,y));
					}
					capture[i][y]=pion[i][y];
				}
				
				if(pion[i][y]=='O') {//orange
					fond[i][y]=new JLabel(orange, JLabel.CENTER);
					fond[i][y].setOpaque(false);
					fond[i][y].setPreferredSize(new Dimension(50,50));
					panneau.add(fond[i][y]);
					if(Fenetre.modedejeu==1) {
						fond[i][y].addMouseListener(new Observateur2(fond[i][y],i,y));
					}
					capture[i][y]=pion[i][y];
				}
			}
		}
		fj.add(pb,BorderLayout.SOUTH);
		fj.add(panneau,BorderLayout.CENTER);
		fj.setVisible(true);

		if(Fenetre.modedejeu>1){
			Thread thread = new Thread(new Threadbot(Fenetre.modedejeu,this, this.size_x, this.size_y));
			thread.start();
			
		}

	}

	/**
	* calcule le score
	*/
	public static void score(int compteur){
		score=compteur;
		if(score>1){
			score-=2;
			score=score*score+point;
		}else{score=point;}
		point=score;
	}

	/**
	* Initialise le compteur
	*/
	public static void setCount(int set){
		compteur=set;
	}

	/**
	* Supprime la subrillance
	*/
	public static void reset(){ 

		for (int i=0;i<size_y;i++){
			for(int y=0;y<size_x;y++){
				fond[i][y].setBackground(Color.WHITE);
				verif[i][y]=0;
			}
		}
	}

	/**
	* Fait tomber les pion vers le bas
	*/
	public static void tomber(){
		for(int wide=0;wide<(size_y-1);wide++){
			for(int i=0;i<(size_y-1);i++){
				for(int y=0;y<size_x;y++){
					if (capture[i+1][y]==' ' && capture[i][y]!=' ') {
						if(capture[i][y]=='R'){
							capture[i+1][y]='R';
							fond[i+1][y].setIcon(banane);
						}
						if(capture[i][y]=='V'){
							capture[i+1][y]='V';
							fond[i+1][y].setIcon(fraise);
						}
						if(capture[i][y]=='B'){
							capture[i+1][y]='B';
							fond[i+1][y].setIcon(peche);
						}
						if(capture[i][y]=='K'){
							capture[i+1][y]='K';
							fond[i+1][y].setIcon(kiwi);
						}
						if(capture[i][y]=='F'){
							capture[i+1][y]='F';
							fond[i+1][y].setIcon(framboise);
						}
						if(capture[i][y]=='O'){
							capture[i+1][y]='O';
							fond[i+1][y].setIcon(orange);
						}
						capture[i][y]=' ';
						fond[i][y].setIcon(vide);
					}
				}
			}
		}
		reset();
	}

	/**
	* Verifie si la ligne est vide
	*/
	public static boolean ligneverif(int i) {
		int j;
		for (j=0; j<(size_x-1); j++) {
			if (capture[i][j]!=' ') {
				return false;
			}
		}
		return true;
	}
	/** 
	* Regarde  si la colonne est vide
	*/
	public static boolean statement(int y){
		for(int i=0;i<size_y;i++){
			if(capture[i][y]!=' '){
				return false;
			}
		}
		return true;
	}
	/**
	* Fait decaler vers la gauche les pions si la colonne est vide
	*/
	public static void decaler(){
		for(int x=0;x<size_y;x++){
			for (int y=0;y<(size_x-1);y++ ) {
				if(statement(y)==true){
					for(int i=0;i<size_y;i++){
						if(capture[i][y+1]=='R'){
							capture[i][y]='R';
							fond[i][y].setIcon(banane);
						}
						if(capture[i][y+1]=='V'){
							capture[i][y]='V';
							fond[i][y].setIcon(fraise);
						}
						if(capture[i][y+1]=='B'){
							capture[i][y]='B';
							fond[i][y].setIcon(peche);
						}
						if(capture[i][y+1]=='K'){
							capture[i][y]='K';
							fond[i][y].setIcon(kiwi);
						}
						if(capture[i][y+1]=='F'){
							capture[i][y]='F';
							fond[i][y].setIcon(framboise);
						}
						if(capture[i][y+1]=='O'){
							capture[i][y]='O';
							fond[i][y].setIcon(orange);
						}
						capture[i][y+1]=' ';
						fond[i][y+1].setIcon(vide);
					}
				}
			}
		}
	}

	/** 
	* Supprime les pions
	*/
	public static void delete(){

		if(compteur>=2){
			int i;
			for(i=0;i<compteur;i++){
				fond[recup[i][0]][recup[i][1]].setIcon(vide);
				capture[recup[i][0]][recup[i][1]]=' ';
			}
		}

		tomber();
		decaler();
		fin();
		if(end==1){
			Endgame end=new Endgame();
			fj.dispose();
		}
	}

	/**
	* Mets en surbrillance les pion selectionner
	*/
	public static void peinture(){
		int i;
		if(compteur>=2){
			for(i=0;i<compteur;i++){
				fond[recup[i][0]][recup[i][1]].setBackground(Color.PINK);
			}

		}
	}

	/**
	* verifie l'existance d'un groupe superieur a un
	*/
	public static void groupe(int x, int y){
		char casesel=capture[x][y];

		fond[x][y].setOpaque(true);
		verif[x][y]=1;
		recup[compteur-1][0]=x;
		recup[compteur-1][1]=y;

		if(y< (size_x-1)) {
			if(capture[x][y+1]==casesel && (y+1<=size_x)){
				if(verif[x][y+1]!=1){
					compteur+=1;
					groupe(x,y+1);	
				}
			}
		}
		if(y > 0) {
			if(capture[x][y-1]==casesel && (y-1>=0)){
				if(verif[x][y-1]!=1){
					compteur+=1;
					groupe(x,y-1);	
				}
			}
		}
		if(x < (size_y-1)) {
			if(capture[x+1][y]==casesel && (x+1<=size_y)){
				if(verif[x+1][y]!=1){
					compteur+=1;
					groupe(x+1,y);
				}
			}
		}

		if(x > 0)  {
			if(capture[x-1][y]==casesel && (x-1>=0)){
				if(verif[x-1][y]!=1){
					compteur+=1;
					groupe(x-1,y);
				}
			}
		}

		if (Fenetre.modeconnex == 2) {
			if(y < (size_x-1) && x < (size_y-1)) {
				if(capture[x+1][y+1] == casesel){
					if(verif[x+1][y+1] != 1){
						compteur += 1;
						groupe(x+1, y+1);
					}
				}
			}
	
			if(y > 0 && x < (size_y-1)) {
				if(capture[x+1][y-1] == casesel){
					if(verif[x+1][y-1] != 1){
						compteur += 1;
						groupe(x+1, y-1);
					}
				}
			}
	
			if(y < (size_x-1) && x > 0) {
				if(capture[x-1][y+1] == casesel){
					if(verif[x-1][y+1] != 1){
						compteur += 1;
						groupe(x-1, y+1);
					}
				}
			}
	
			if(y > 0 && x > 0) {
				if(capture[x-1][y-1] == casesel){
					if(verif[x-1][y-1] != 1){
						compteur += 1;
						groupe(x-1, y-1);
					}
				}
			}
		}
	}

	@Override
	public char[][] getGrille(){
		return capture;
	}

	@Override
	public void selectGroup(int x, int y){

		char casesel=capture[x][y];
		setCount(1);
		fond[x][y].setOpaque(true);
		verif[x][y]=1;
		recup[compteur-1][0]=x;
		recup[compteur-1][1]=y;

		if(y< (size_x-1)) {
			if(capture[x][y+1]==casesel && (y+1<=size_x)){
				if(verif[x][y+1]!=1){
					compteur+=1;
					groupe(x,y+1);	
				}
			}
		}
		if(y > 0) {
			if(capture[x][y-1]==casesel && (y-1>=0)){
				if(verif[x][y-1]!=1){
					compteur+=1;
					groupe(x,y-1);	
				}
			}
		}
		if(x < (size_y-1)) {
			if(capture[x+1][y]==casesel && (x+1<=size_y)){
				if(verif[x+1][y]!=1){
					compteur+=1;
					groupe(x+1,y);
				}
			}
		}

		if(x > 0)  {
			if(capture[x-1][y]==casesel && (x-1>=0)){
				if(verif[x-1][y]!=1){
					compteur+=1;
					groupe(x-1,y);
				}
			}
		}


	}
	@Override
	public void destroyGroup(){

		if(compteur>=2){
			int i;
			for(i=0;i<compteur;i++){
				fond[recup[i][0]][recup[i][1]].setIcon(vide);
				capture[recup[i][0]][recup[i][1]]=' ';

			}
		}

		tomber();
		decaler();
		if(getFini()==false){
			Endgame end=new Endgame();
			fj.dispose();
		}
		score(getCompteur());

		try{
			Thread.sleep(200);
		}catch(InterruptedException e1){}

	}
	@Override
	public int getScore(){
		return this.compteur=1;
	}
	@Override
	public int getTotalScore(){
		return this.compteur;
	}
	@Override
	public boolean getFini(){
		int i,j;
		boolean status,win;
		win=false;
		for (i=0;i<size_y;i++){
			status=ligneverif(i);
			if(status==false){
				for(j=0;j<size_x;j++){
					if(capture[i][j]!=' '){
						if(i>0 && capture[i][j]==capture[i-1][j] || i<(size_y-1) && capture[i][j]==capture[i+1][j]){
							return true;
						}

						if(j>0 && capture[i][j]==capture[i][j-1] || j<(size_x-1) && capture[i][j]==capture[i][j+1]){
							return true;
						}
					}
				}
			}
		}
		if(win==false){
			return false;
		}
		return true;
	}

	@Override
	public void unselectAll(){

	}
}