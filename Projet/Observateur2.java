import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
* La classe <code>Observateur2</code> Observateur de la souris.
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Observateur2 implements MouseListener{
	private JLabel image =new JLabel();
	private int x;
	private int y;
	private int count=0;

	/**
	* Constructeur de la class qui a besoin des locations, des pauses et d'un JLabel
	*/
	public Observateur2(JLabel im,int a,int b){
		this.image=im;
		this.x=a;
		this.y=b;
	}

	/**
	* Supprime le groupe lors du clique de la souris
	*/
	public void mouseClicked(MouseEvent evenement){
		if (count==0){
			Ecran.delete();
			count++;
			Ecran.score(Ecran.getCompteur());
		}
	}

	/**
	* Mets un groupe en surbrillance à son contact      
	*/
	public void mouseEntered(MouseEvent evenement){
		if(Ecran.capture[x][y]!=' '){
			Ecran.setCount(1);
			Ecran.groupe(x,y);
			Ecran.peinture();
			count=0;
		}
	}          
	/**
	* Enleve la surbrillance
	*/
	public void mouseExited(MouseEvent evenement){
		Ecran.reset();
		image.setOpaque(false);
	}          
	public void mousePressed(MouseEvent evenement){}
	public void mouseReleased(MouseEvent evenement){}         
}