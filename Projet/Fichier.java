import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;

/**
* La classe <code>Fichier</code> Crée un tableau à l'aide de la lécture de fichier
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Fichier{
	/*
	* Crée un attribut
	*/
	private File niveau;
	/**
	* Tableau de caractere pour attribué les caractère
	*/
	public static char [][] jeux = new char[10][15];
	/**
	* initialise la lecture de fichier a null
	*/
	public static  BufferedReader buffer=null;
	/**
	* Lis et attribut les caractère d'un fichier à un tableau
	*/
	public Fichier(File choix){
		int z=0;
		niveau=choix;
		String ligne;

		try {
			buffer = new BufferedReader(new FileReader(niveau));
			try {
				while((ligne=buffer.readLine())!=null && z<10){	
					char[] caractere=ligne.toCharArray();
					for(int y=0;y<15;y++){
						jeux[z][y]=caractere[y];
					}
					z++;
				}
			} catch(IOException e) {
				System.err.println("Impossible de lire dans le fichier reels.bin !");
			}
			try {
				buffer.close();
			} catch(IOException e) {
				System.err.println("Impossible de fermer le fichier reels.bin !");
			}
		} catch(FileNotFoundException e) {
			System.err.println("Impossible d'ouvrir le fichier reels.bin en lecture !");
		}
	}
}



