import java.awt.event.*;

/**
* La classe <code>Obp</code> Observateur de la class pause
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Obp implements ActionListener{
	/**
	* Appel la class pause
	*/
	private Pause pause;

	/**
	* Constructeur initialise pause
	*/
	public Obp(Pause pause){
		this.pause=pause;
	}
	/**
	* Mets en observation les boutons de pause qui permettes de reprendre ou de quitter la partie
	*/
	public void actionPerformed(ActionEvent eve){
		this.pause.count=0;
		if(eve.getSource()==this.pause.resume){
			this.pause.pj.dispose();
		}
		if(eve.getSource()==this.pause.exit){
			Jeux.fenetre.principal();
			this.pause.pj.dispose();
			this.pause.subs.dispose();
		}
	}
}