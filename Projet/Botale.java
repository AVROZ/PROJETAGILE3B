import javax.swing.*;
import javax.swing.ImageIcon;
import java.awt.*;
import java.util.Random;
// Exemple d'utilisation des interfaces
/*
Les méthodes du bot seront appelées dans le moteur de jeu.

*/
public class Botale implements BotInterface {

	private API ecran;
	private int x,y;
		/**
	* random
	*/
	private Random random = new Random();

	/**
	*capture les pion selectionner pour d'autre méthode
	*/
	public char[][] capture = new char[10][15];

	public Botale() {
		
		}
	

	// A lancer en premier pour que les autres méthodes fonctionnent
	public void setAPI(API interface_moteur_jeu) {
		this.ecran = interface_moteur_jeu;
	}


	public void startBot(int size_x, int size_y) {
		this.capture = this.ecran.getGrille();	
		while(this.ecran.getFini()){
				do{
					this.x=(1+random.nextInt(size_y))-1;
					this.y=(1+random.nextInt(size_x))-1;
				}while(this.capture[this.x][this.y]==' ');
					this.ecran.selectGroup(this.x,this.y);
					this.ecran.destroyGroup();
					this.capture = this.ecran.getGrille();		
			}
	}

}



