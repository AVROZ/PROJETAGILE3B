import java.util.Random;

/**
* La classe <code>Grille</code> class qui remplie le tableau de jeux de facon alÃ©atoire
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Grille  {
	/** 
	* Tableau de pion 
	*/
	public char[][] pion;

	/** 
	* Remplie la grille 
	*/
	public Grille(int size_x, int size_y, int nb_pion){
		int rnd;
		pion = new char[size_y][size_x];
		Random random = new Random();
		char[] setcol={'R','V','B','K','F','O'};

		for(int i=0;i<size_y;i++){
			for(int j=0;j<size_x;j++){
				rnd=random.nextInt(nb_pion);
				pion[i][j] = setcol[rnd];
			}
		}
	}
}