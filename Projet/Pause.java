import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
* La classe <code>Pause</code> Class qui gère l'écran de pause
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Pause {

	/**
	*fond du menu pause
	*/
	private Image fond = Toolkit.getDefaultToolkit().getImage("./image/pause.jpg");
	/**
	*boutton qui reprend le jeu
	*/
	public static JButton resume = new JButton("resume");
	/**
	*bouttons pour quitter la partie
	*/
	public static JButton exit = new JButton("exit");
	/**
	* Ecran de pause
	*/
	public static JDialog pj = new JDialog();
	/**
	* Compteur qui empêche de créer plusieurs menus
	*/
	public static int count = 0;
	/**
	*panneau ou se trouve les boutton
	*/
	private JPanel panneau = new JPanel();
	public static JFrame subs = new JFrame();
	/**
	* Panneau où l'on met le fond
	*/
	private JPanel fimage = new JPanel(){
		protected void paintComponent(Graphics pinceau){
			pinceau.drawImage(fond, 0, 0, this);
		}
	};

 	/**
 	* Gère l'affichage de la fenêtre
	*/
	public Pause(JFrame fenetre){
		subs=fenetre;
		exit.addActionListener(new Obp(this));
		resume.addActionListener(new Obp(this));
	}

	public void menu(){
		count++;
		if(count==1){
			pj.setTitle("Same Game");
			pj.setIconImage(Toolkit.getDefaultToolkit().getImage("./image/Kitty_kitty.png"));
			Color lila=Fenetre.lila;	
			pj.setSize(600, 500);
			pj.setResizable(false);
			pj.setLocation(100,100);
			resume.setBackground(lila);
			exit.setBackground(lila);
			pj.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
			pj.setModal(true);
			panneau.add(resume);
			panneau.add(exit);
			pj.add(fimage,BorderLayout.CENTER);
			pj.add(panneau, BorderLayout.SOUTH);
			pj.setVisible(true);
		}
	}
}
