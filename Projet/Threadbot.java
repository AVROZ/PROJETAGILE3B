import javax.swing.*;
import javax.swing.ImageIcon;
import java.awt.*;



public class Threadbot implements Runnable{
	private API moteur;
	private int mode;
	private int size_x;
	private int size_y;
	
	public Threadbot(int modedejeu, API moteurjeu, int size_x, int size_y){
		this.moteur=moteurjeu;
		this.mode=modedejeu;
		this.size_x = size_x;
		this.size_y = size_y;
	}

	@Override
	public void run(){

		if(this.mode==2){
			Botale botale=new Botale();
			botale.setAPI(this.moteur);
			botale.startBot(this.size_x, this.size_y);
		}


		if(this.mode==3){
			Botglouton bot=new Botglouton();
			bot.setAPI(this.moteur);
			bot.startBot(this.size_x, this.size_y);


		}
	}
}


