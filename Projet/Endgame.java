import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
* La classe <code>Endgame</code> Class qui affiche l'écran de fin
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Endgame{
    /**
    * Ecran de fin
	*/
	public static JFrame endgame;
	/**
	* Boutton qui retourn au menu
	*/
	public static JButton menu = new JButton("menu");
	/**
	* Fond du menu
	*/
	private static Image fond = Toolkit.getDefaultToolkit().getImage("./image/Fin.png");
	/**
	* Panneau contenant le fond
	*/
	private JPanel pimage = new JPanel(){
		protected void paintComponent(Graphics pinceau){
			pinceau.drawImage(fond, 0, 0, this);
		}
	};

	/**
	* Gère l'affichage du menu de fin à l'écran
	*/
	public Endgame(){
		this.endgame=new JFrame();
		String score=Integer.toString(Ecran.score);
		JLabel point=new JLabel("score: "+score);
		JPanel pan1=new JPanel();
		JPanel pan2=new JPanel();
		endgame.setTitle("Same Game");
		endgame.setIconImage(Toolkit.getDefaultToolkit().getImage("./image/Kitty_kitty.png"));
		endgame.setSize(500, 500);
		endgame.setResizable(false);
		endgame.setLocation(100, 100);
		endgame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pan2.add(menu);
		pan2.add(point);
		endgame.add(pimage, BorderLayout.CENTER);
		endgame.add(pan2, BorderLayout.SOUTH);
		menu.addActionListener(new Obend());
		endgame.setVisible(true);
	}
}