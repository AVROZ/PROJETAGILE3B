import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
* La classe <code>Fenetre</code> Fenêtre du menu principal
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Fenetre {

	/**
	*couleur pour mes boutons
	*/
	public static Color lila = new Color(200, 162, 200);
	/**
	* Panneau où mettre ma grille de boutons
	*/
	private JPanel gimage = new JPanel();
	/**
	* Grille 1 ligne 3 colone pour placer mes bouton
	*/
	private GridLayout grille = new GridLayout(1, 4);
	/**
	* Bouton liée à la création d'une grille aléatoire
	*/
	public static JButton play=new JButton("Random");
	/**
	* Bouton liée a la création de grille prédéfinie
	*/
	public static JButton setting=new JButton("Select file");
	/**
	 * Bouton liée au paramétrage de la partie
	 */
	public static JButton parameter=new JButton("Settings");
	/**
	* Bouton de sortie de jeu
	*/
	public static JButton exit=new JButton("exit");
	/**
	* Fenetre du menu
	*/
	public static JFrame fenetre = new JFrame();
	/**
	* Bouton liée à l'utilisation de bot aléatoire
	*/
	public static JCheckBox bota=new JCheckBox("Bot aleatoire");
	/**
	* Bouton liée à l'utilisation de bot glouton
	*/
	public static JCheckBox botg=new JCheckBox("Bot glouton");
	/**
	* Bouton liée à l'utilisation du jeu par le joueur
	*/
	public static JCheckBox joueur=new JCheckBox("Joueur");
	/**
	* Groupe bouton liée à l'utilisation de bot
	*/
	public static ButtonGroup group = new ButtonGroup();
	/**
	* Bouton liée à la connexité
	*/
	public static JCheckBox connex = new JCheckBox("Connexité");
	/**
	* Le fond de mon menu
	*/
	private Image fond = Toolkit.getDefaultToolkit().getImage("./image/menu.jpg");
	/**
	* valeur en fonction du mode jeux
	*/
	public static int modedejeu=1;
	/**
	* valeur en fonction de la connexité
	*/
	public static int modeconnex=1;
	/**
	* Atribution de mon fond au paneau
	*/
	private JPanel pimage = new JPanel(){
		protected void paintComponent(Graphics pinceau){
			pinceau.drawImage(fond, 0, 0, this);
		}
	};

	/**
	* Constructeur où se trouve les observateurs
	*/
	public Fenetre(){
		this.play.addActionListener(new Obf(this));
		this.exit.addActionListener(new Obf(this));
		this.bota.addActionListener(new Obf(this));
		this.botg.addActionListener(new Obf(this));
		this.joueur.addActionListener(new Obf(this));
		this.connex.addActionListener(new Obf(this));
		this.setting.addActionListener(new Obs());
		this.parameter.addActionListener(new Obf(this));
	}

	/**
	* Méthode pour l'affichage de la fenêtre
	*/
	public void principal(){

		this.fenetre.setTitle("Same Game");
		this.fenetre.setIconImage(Toolkit.getDefaultToolkit().getImage("./image/Kitty_kitty.png"));
		this.fenetre.setSize(500, 500);
		this.fenetre.setResizable(false);
		this.fenetre.setLocation(0, 0);
		this.fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.gimage.add(play);
		this.play.setBackground(lila);
		this.gimage.add(setting);
		this.setting.setBackground(lila);
		this.gimage.add(parameter);
		this.parameter.setBackground(lila);

		
		this.group.add(joueur);
		this.group.add(bota);
		this.group.add(botg);
		this.gimage.add(exit);
		this.pimage.add(joueur);
		this.pimage.add(bota);
		this.pimage.add(botg);
		if(modedejeu==1){
			this.joueur.setSelected(true);
		}
		if(modedejeu==2){
			this.bota.setSelected(true);
		}
		if(modedejeu==3){
			this.botg.setSelected(true);
		}
		this.exit.setBackground(lila);
		this.gimage.setLayout(grille);
		this.fenetre.add(pimage, BorderLayout.CENTER);
		this.fenetre.add(gimage, BorderLayout.SOUTH);
		this.fenetre.add(connex, BorderLayout.WEST);
		this.fenetre.setVisible(true);
	}
}