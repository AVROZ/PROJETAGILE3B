import javax.swing.*;
import java.awt.event.*;

/**
* La classe <code>Obf</code> Observateur de la class menu
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Obf implements ActionListener{
	/**
	* Créer une classe fenetre
	*/
	private Fenetre fenetre;
	/**
	* Créer une classe écran
	*/
	private Ecran ecran;
	/**
	* Constructeur qui prend une fenêtre en paramêtre
	*/
	public Obf(Fenetre fenetre){
		this.fenetre=fenetre;
	}
	/**
	* Observe les boutons play et exit du menu principal
	*/
	@Override
	public void actionPerformed(ActionEvent eve){
		if(eve.getSource()==this.fenetre.play){
			Ecran.choix=0;
			this.ecran=new Ecran();
			this.fenetre.fenetre.dispose();   
		}
		if(eve.getSource()==this.fenetre.exit){
			this.fenetre.fenetre.dispose();
		}
		
		if(eve.getSource()==this.fenetre.joueur){
			if (this.fenetre.joueur.isSelected()) {
				this.fenetre.modedejeu=1;
				System.out.println(this.fenetre.modedejeu);
			}
		}

		if(eve.getSource()==this.fenetre.bota){
			if (this.fenetre.bota.isSelected()) {
				this.fenetre.modedejeu=2;
				System.out.println(this.fenetre.modedejeu);
			}
		}

		if(eve.getSource()==this.fenetre.botg){
			if (this.fenetre.botg.isSelected()) {
				this.fenetre.modedejeu=3;
				System.out.println(this.fenetre.modedejeu);
			}
		}
		
		if(eve.getSource()==this.fenetre.connex){
			if (this.fenetre.connex.isSelected()) {
				this.fenetre.modeconnex=2;
				System.out.println(this.fenetre.modeconnex);
			}
			else {
				this.fenetre.modeconnex=1;
				System.out.println(this.fenetre.modeconnex);
			}
		}
		
		if(eve.getSource()==this.fenetre.parameter) {
			Integer[] sizeToChoose = new Integer[16];
			Integer[] colorsToChoose = new Integer[6];
			Integer nb_pion;
			
			for(int i=5, j=0; i<=20; i++, j++) {
				sizeToChoose[j] = i;
			}

	        Integer getSize = (Integer) JOptionPane.showInputDialog(
	                null,
	                "Horizontal size",
	                "Settings",
	                JOptionPane.QUESTION_MESSAGE,
	                null,
	                sizeToChoose,
	                sizeToChoose[10]);
	        
	        this.ecran.size_x = getSize;
	        
	        getSize = (Integer) JOptionPane.showInputDialog(
	                null,
	                "Vertical size",
	                "Settings",
	                JOptionPane.QUESTION_MESSAGE,
	                null,
	                sizeToChoose,
	                sizeToChoose[5]);
	        
	        this.ecran.size_y = getSize;
	        
	        for(int i=0; i<6; i++) {
	        	colorsToChoose[i] = i+1;
	        }
	        
	        nb_pion = (Integer) JOptionPane.showInputDialog(
	                null,
	                "Number of colors",
	                "Settings",
	                JOptionPane.QUESTION_MESSAGE,
	                null,
	                colorsToChoose,
	                colorsToChoose[2]);
	        
	        this.ecran.nb_pion = nb_pion;
		}
	}
}