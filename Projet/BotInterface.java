/**
* Interface : BotInterface
* Interface a implémenter dans vos bot, le bot sera initialisé dans le moteur de jeu et ses méthodes seront lancés dans le moteur de jeu
* @version 1.0
* @author Le Conseil des 4
*/
interface BotInterface {

	/**
	* Méthode permettant d'appliquer une API de moteur de jeu au bot 
	*/
	public abstract void setAPI(API interface_moteur_jeu);

	/**
	* Méthode permettant de lancer la partie d'un bot
	*/
	public abstract void startBot(int size_x, int size_y);

}