import javax.swing.*;
import java.io.*;
import java.awt.event.*;

/**
* La classe <code>Setting</code> Gère le bouton des fichiers
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Setting {
    /**
    * boutton files
	*/
	public static JButton files=new JButton();
    /**
    * Crée un attribut de type file
	*/
	public static File file;
	/**
	* Mets en observation le bouton "files"
	*/
	public Setting(){
		files.addActionListener(new Obs());
	}
}