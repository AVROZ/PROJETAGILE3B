import javax.swing.*;
import java.awt.event.*;

/**
* La classe <code>Obe</code> Observateur de la class écran.
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Obe implements ActionListener{
     public static JFrame fenetre=new JFrame();
	/**
     * constructeur qui initialise un JFrame
     */
     public Obe(JFrame f){
          fenetre=f;
     }

     /**
     * appel la class pause si le boutton est pressé
     */
     @Override   	
     public void actionPerformed(ActionEvent eve){
          if(eve.getSource()==Ecran.pause){
               Pause pause=new Pause(fenetre);
               pause.menu();
          }
     }
}