import javax.swing.*;
import java.util.Scanner;
import java.awt.event.*;
import javax.swing.JFileChooser;

/**
* La classe <code>Obs</code> Observateur de la class setting
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Obs implements ActionListener{
	private Fichier files;
	@Override
	/**
	* Affiche la sélection des fichiers et l'envoie à la class fichier
	*/
	public void actionPerformed(ActionEvent eve){
		if(eve.getSource()==Fenetre.setting){
			int rep;
			Ecran.choix=1;
			Scanner fileIn;
			JFileChooser chooser = new JFileChooser("./niveaux");
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			rep=chooser.showOpenDialog(null);
			Setting.file=chooser.getSelectedFile();
			this.files=new Fichier(Setting.file);
			Fenetre.fenetre.dispose();
			Ecran fichier=new Ecran();
		}
	}
}