import javax.swing.*;
import javax.swing.ImageIcon;
import java.awt.*;
import java.util.Random;
// Exemple d'utilisation des interfaces
/*
Les méthodes du bot seront appelées dans le moteur de jeu.

*/
public class Botglouton implements BotInterface {

	private API ecran;
	private int x,y;
	private int size_x, size_y;
	private int compteur=0;
	private int oldcompteur=0;
	/**
	* random
	*/
	private Random random = new Random();
	public int[] tab = new int[2];

	/**
	*capture les pion selectionner pour d'autre méthode
	*/
	public char[][] capture;
	public char[][] verif;
	public char couleur;

	public Botglouton() {
		
	}
	

	// A lancer en premier pour que les autres méthodes fonctionnent
	public void setAPI(API interface_moteur_jeu) {
		this.ecran = interface_moteur_jeu;
	}


	public void startBot(int sizex, int sizey) {
		this.capture = this.ecran.getGrille();
		this.size_x = sizex;
		this.size_y = sizey;
		this.capture = new char[this.size_y][this.size_x];
		this.verif = new char[this.size_y][this.size_x];

		while(this.ecran.getFini()){



			for(this.x=0;this.x<size_y;this.x++){

				for(this.y=0;this.y<size_x;this.y++){

					for (int a=0;a<size_y ;a++ ) {
						for (int b=0;b<size_x ;b++ ) {
							verif[a][b]=0;

						}

					}
					if((this.couleur=this.capture[this.x][this.y])!=' '){


						bot(this.x,this.y);
						if (this.compteur>this.oldcompteur){
							this.oldcompteur=this.compteur;
							this.tab[0]=this.x;
							this.tab[1]=this.y;

						}
					}
				}
			}

			this.ecran.selectGroup(this.tab[0],this.tab[1]);
			this.ecran.destroyGroup();
			this.capture = this.ecran.getGrille();		
		}
	}

	public void bot(int x, int y){

		this.verif[x][y]=1;
		
		if(y< (size_x-1)) {
			if((y+1<=size_x)){
				if(this.verif[x][y+1]!=1 && this.couleur == capture[x][y+1]){
					this.compteur+=1;
					bot(x,y+1);	
				}
			}
		}
		if(y > 0) {
			if((y-1>=0)){
				if(this.verif[x][y-1]!=1 && this.couleur == capture[x][y-1]){
					this.compteur+=1;
					bot(x,y-1);	
				}
			}
		}
		if(x < (size_y-1)) {
			if((x+1<=size_y)){
				if(verif[x+1][y]!=1 && this.couleur == capture[x+1][y]){
					this.compteur+=1;
					bot(x+1,y);
				}
			}
		}

		if(x > 0)  {
			if((x-1>=0)){
				if(this.verif[x-1][y]!=1 && this.couleur == capture[x-1][y]){
					this.compteur+=1;
					bot(x-1,y);
				}
			}
		}

	}


	

}