import javax.swing.*;
import java.awt.*;

/**
* La classe <code>Jeux</code> Lance le jeu
* @version 1.0
* @author Abdel BEN REJEB
* @author Emre TURKAN
*/

public class Jeux {
	public static Fenetre fenetre = new Fenetre();
	public static void main(String[] args) {
		fenetre.principal();
	}
}